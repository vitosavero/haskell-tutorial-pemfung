maxTiga :: Int -> Int -> Int -> Int
maxTiga x y z = max(max x y) z
--  maxTiga 57 100 90
-- 100
--


kpk :: Int -> Int -> Int
kpk x y = head [z | z <- [x, (x+x) ..], z `mod` y == 0]
-- kpk 7 5
-- 35
--

fpb :: Integer -> Integer -> Integer
fpb a b | b == 0 = abs a | otherwise = fpb b (a `mod` b)
-- >>> fpb 2 9
-- 1
--

module Main where

   import Control.Monad
   import Control.Monad.Trans (liftIO)
   import Happstack.Server.Internal.Monads (anyRequest)
   import Happstack.Server.SimpleHTTP
   import Happstack.Server.FileServe
   import Numeric
   import Contracts
   
   instance FromData ExContr where
     fromData = do c    <- look "contract"
                   arg1 <- look "arg1"
                   arg2 <- look "arg2"
                   img  <- look "image" 
                   return $ ExContr (c, map fst $ readFloat arg1
                                               ++ readFloat arg2, read img)
   
   main :: IO ()
   main = do
     simpleHTTP (nullConf { port = 80 }) $ msum [
            dir "contractEx" $ withData $ \(ExContr t) -> msum $ [
                   anyRequest $ fmap toResponse $ liftIO $ renderEx (ExContr t)
                 , anyRequest $ ok $ toResponse renderExDefault
                 ]
          , serveDirectory DisableBrowsing ["ComposingContracts.html"] "public"
          ] 
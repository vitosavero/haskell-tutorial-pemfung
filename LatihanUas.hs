import Data.List 
import qualified Data.Char as Char

-- Kelas A Soal 1
-- --------------

-- Given a list of words, remove from the list all those which contain four or more vowels 
-- and those which have same letter appearing twice (or more) in a row.
-- In addition, any word containing numbers should have the numbers removed. (Done)
-- Note that the number removal should happen before any other operations so that
-- the subsequent operations can remove the word if necessary. 
-- (source: https://www.fer.unizg.hr/)

-- weirdFilter :: [String] -> [String]
-- weirdFilter ["ab3c", "bananae", "fuzzy", "c1c2"]  
-- ["abc"]

-- Ngilangin numbers

notNumbers x = x `notElem` "1234567890"
removeNumber = filter notNumbers

isNotAppearTwice (x:y:ls) | (x==y)   = False
isNotAppearTwice (x:ls)              = isNotAppearTwice ls
isNotAppearTwice []                  = True

isVowel x = x `elem` "aiueoAIUEO"
countVowel = length . filter isVowel 
isThreeOrLess x = countVowel x < 4

weird = filter isThreeOrLess . filter isNotAppearTwice . map removeNumber

-- weird ["ab3c", "bananae", "fuzzy", "c1c2"]
-- ["abc"]

-- Kelas A Soal 2
-- --------------
-- Write a function rotabc that changes a's to b's, b's to c's and c's to a's in a string.
-- Only lowercase letters are affected. (source: https://www2.cs.arizona.edu/classes/cs372/spring14)

-- rotabc :: String -> String
-- rotabc "Bananae"  
-- Bbnbnbe

-- found "hole" error on last condition
-- changeabc (x:xs)   
--         | x == 'a' = 'b'    : changeabc xs
--         | x == 'b' = 'c'    : changeabc xs
--         | x == 'c' = 'a'    : changeabc xs
--         | _  = x : changeabc xs

changeabc = map abc
    where   abc 'a' = 'b'
            abc 'b' = 'c'
            abc 'c' = 'a'
            abc x = x

-- changeabc "ahdbnmc"
-- "bhdcnma"

-- Kelas A Soal 3
-- --------------

-- Definisikan fungsi last, dengan menerapkan point-free style.
-- Fungsi last tersebut menerima sebuah list 
-- dan mengembalikan elemen terakhir dari list tersebut.

lastA = head . reverse
-- lastA "haskel"
-- 'l'

-- Kelas B Soal 1
-- --------------

-- Given a sentence, define a function called capitalise which returns
--the same sentence with all words capitalised 
-- except the ones from a given list.
-- (source: https://www.fer.unizg.hr/)

-- capitalise :: String -> [String] -> String
-- capitalise "this is a sentence." ["a", "is"]
-- "This is a Sentence."

-- get the first word
getWord word [] = (word,[])
getWord word (x:xs) | x == ' ' = (word, xs)
                    | x /= ' ' = getWord (word ++ [x]) xs

-- getWord [] "this is a sentence."
-- ("this","is a sentence.")

-- create list of string from string
splitter lw []  = lw
splitter lw inp = let (word, rest) = getWord "" inp
                  in splitter (lw ++ [word]) rest

-- splitter [] "this is a sentence."
-- ["this","is","a","sentence."]

-- Make the first word to capitalize if word not in list
upper el word@(h:t) =  if word `elem` el 
                       then word 
                       else Char.toUpper h : t

-- upper ["is","a"] "this"
-- "This"

-- combine the list of string to a string
combine []  = []
combine [a] = a
combine (a:xs) = a ++ " " ++ (combine xs)

-- combine ["this","is","a","sentence."]
-- "this is a sentence."

capitalise el inp = combine (map (upper el) (splitter [] inp)) 

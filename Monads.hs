-- (*) <$> Just 2 <*> Just 8
-- Just 16
--

-- (++) <$> Just "klingon" <*> Nothing
-- Nothing
--

-- (-) <$> [3,4] <*> [1,2,3]
-- [2,1,0,3,2,1]
--

-- fmap (++"!") (Just "wisdom")
-- Just "wisdom!"
--

-- fmap (++"!") Nothing
-- Nothing
--

-- Just (+3) <*> Just 3
-- Just 6
--

-- Nothing <*> Just "greed"
-- Nothing
--

type Birds = Int
type Pole = (Birds,Birds)

landLeft :: Birds -> Pole -> Pole  
landLeft n (left,right) = (left + n,right)  
  
landRight :: Birds -> Pole -> Pole  
landRight n (left,right) = (left,right + n)  

-- landLeft 2 (0,0)
-- (2,0)
--

-- landRight (-1) (1,2)
-- (1,1)
--

-- landLeft 2 (landRight 1 (landLeft 1 (0,0)))
-- (3,1)

-- >>> landRight . 1 landLeft 1 (0, 0)

f x y = x + y
g x y = x * y
-- >>> g 2 3
-- 6
-- >>> f 1 2
-- 3
-- >>>f 1 (g 1 2)
-- 3
-- >>> f 1 $ g 2 3
-- 7
--



-- >>> 100 -: (*3)


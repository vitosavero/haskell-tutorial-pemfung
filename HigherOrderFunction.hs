multThree :: (Num a) => a -> a -> a -> a  
multThree x y z = x * y * z

compareWithHundred :: (Num a, Ord a) => a -> Ordering  
compareWithHundred = compare 100

divideByTen :: (Floating a) => a -> a  
divideByTen = (/10)

isUpperAlphanum :: Char -> Bool  
isUpperAlphanum = (`elem` ['A'..'Z'])

-- isBetweenTen :: (Num a) => a -> ([a]) -> Bool
-- isBetweenTen x = map && True ( filter (<10) [1..x])

-- applyTwice :: (a -> a) -> a -> a  
-- applyTwice f x = f (f x)

-- applyTwice (++ " woi") "asik" = (++ " woi") ((++ " woi") "asik")

zipWith' :: (a -> b -> c) -> [a] -> [b] -> [c]  
zipWith' _ [] _ = []  
zipWith' _ _ [] = []  
zipWith' f (x:xs) (y:ys) = f x y : zipWith' f xs ys

-- filter' :: (a -> Bool) -> [a] -> [a]  
-- filter' _ [] = []  
-- filter' p (x:xs)   
--     | p x       = x : filter' p xs  
--     | otherwise = filter' p xs

quicksort :: (Ord a) => [a] -> [a]    
quicksort [] = []    
quicksort (x:xs) =     
    let smallerSorted = quicksort (filter (<=x) xs)  
        biggerSorted = quicksort (filter (>x) xs)   
    in  smallerSorted ++ [x] ++ biggerSorted

largestDivisible :: (Integral a) => a  
largestDivisible = head (filter p [100000,99999..])  
    where p x = x `mod` 3829 == 0

chain :: (Integral a) => a -> [a]  
chain 1 = [1]  
chain n  
    | even n =  n:chain (n `div` 2)
    | odd n  =  n:chain (n*3 + 1)

numLongChains :: Int
numLongChains = length (filter (\xs -> length xs > 15) (map chain [1..100]))

addThree :: (Num a) => a -> a -> a -> a  
addThree = \x -> \y -> \z -> x + y + z

-- flip' :: (a -> b -> c) -> b -> a -> c  
-- flip' f x y = f y x 

flip' :: (a -> b -> c) -> b -> a -> c  
flip' f = \x y -> f y x

sum' :: (Num a) => [a] -> a
-- sum' xs = foldl (\acc x -> acc + x) 0 xs
sum' = foldl (+) 0

elem' :: (Eq a) => a -> [a] -> Bool  
elem' y ys = foldl (\acc x -> if x == y then True else acc) False ys

map' :: (a -> b) -> [a] -> [b]  
-- map' _ [] = []  
-- map' f (x:xs) = f x : map' f xs
-- map' f xs = foldr (\x acc -> f x : acc) [] xs  
map' f = foldr (\x acc -> f x : acc) []
-- map' f xs = foldl (\acc x -> acc ++ [f x]) [] xs

maximum' :: (Ord a) => [a] -> a
maximum' = foldr1 (\x acc -> if x > acc then x else acc)
-- maximum' [2,4,5,10,9,8]
-- 10

reverse' :: [a] -> [a]
reverse' = foldl (\acc x -> x : acc) []
-- reverse'[1,4,5,7,3,4,9]
-- [9,4,3,7,5,4,1]

product' :: (Num a) => [a] -> a
product' = foldr1 (*)
-- >>> product'[4,5,10]
-- 200

filter' :: (a -> Bool) -> [a] -> [a]
filter' p = foldr (\x acc -> if p x then x : acc else acc) []
-- filter (<=4) [1,2,3,4,5,6,7,8]
-- [1,2,3,4]
--

head' :: [a] -> a  
head' = foldr1 (\x _ -> x)
-- head' [1,3,4]
-- 1
--

last' :: [a] -> a
last' = foldl1 (\_ x -> x)
-- last'[9,4,5,2,6]
-- 6
--

-- scanl (+) 0 [3,5,2,1]
-- [0,3,8,10,11]
--

sqrtSums :: Int
sqrtSums = length (takeWhile (<1000) (scanl1 (+) (map sqrt [1 ..]))) + 1
-- sqrtSums
-- 131
--

-- map ($ 3) [(+4), (*10), (^2), sqrt]
-- [7.0,30.0,9.0,1.7320508075688772]
--

-- ($) :: (a -> b) -> a -> b  
-- f $ x = f x  

-- (.) :: (b -> c) -> (a -> b) -> a -> c  
-- f . g = \x -> f (g x)

-- map (negate . abs) [5,-3,-6,7,-3,2,-19,24]
-- [-5,-3,-6,-7,-3,-2,-19,-24]

-- fn x = ceiling (negate (tan (cos (max 50 x))))
-- fn 90
-- 1
--
fn = ceiling . negate . tan . cos . max 50
-- fn 90
-- 1
--

oddSquareSum :: Integer
-- oddSquareSum = sum (takeWhile (<10000) (filter odd (map (^2) [1..])))
-- oddSquareSum
-- 166650
--

-- oddSquareSum = sum . takeWhile (<10000) . filter odd . map (^2) $ [1..]
-- oddSquareSum
-- 166650
--
oddSquareSum =
    let oddSquares = filter odd $ map (^2) [1..]
        belowLimit = takeWhile (<10000) oddSquares
    in  sum belowLimit
-- oddSquareSum
-- 166650
--
-- >>> takeWhile (<4) [1,2,3,4,5]
-- [1,2,3]
--

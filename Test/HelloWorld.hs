module Main where

import Control.Monad (msum)
import Data.Char (toLower)
import Happstack.Server (FromReqURI(..), nullConf, simpleHTTP, toResponse, ok, dir, path)

main :: IO()
main = simpleHTTP nullConf $ msum   [   dir "hello" $ path $ \s -> ok $ "Hello, " ++ s
                                    ]

--1
-- Mengeluarkan list hasil pertamabahan dari x dan y dimana
-- x dimasukkan nilai 1 sampai 4, y dimasukkan nilai 2 sampai 4,
-- dan x lebih besar dari y 

-- 2
divisor n = [ns | ns <- [1 .. n], (n `mod` ns) == 0]
-- divisor 12
-- [1,2,3,4,6,12]
--

-- 3
quicksort :: (Ord a) => [a] -> [a]
quicksort [] = []
quicksort (x:xs) = smallerSorted ++ [x] ++ biggerSorted
    where   smallerSorted = quicksort [a | a <- xs, a <= x]
            biggerSorted = quicksort [a | a <- xs, a > x]

-- quicksort [2,3,5,4,2,7,4,9,7,5]
-- [2,2,3,4,4,5,5,7,7,9]
--

-- 4
factorial x = foldr (*) 1 [1..x]

permutation n k = (factorial n) `div` (factorial (n-k))
-- permutation 6 9
-- 720
--


-- 5
primas = sieve [2..]
    where sieve (x:xs) = x : sieve [y | y<-xs, y `mod` x /=0]
-- take 10 primas
-- [2,3,5,7,11,13,17,19,23,29]
--

-- 6
pythaTriple :: (Eq a, Integral a) => [(a, a, a)]
pythaTriple = [(x, y, z) | z <- [5 ..], y <- [4 .. z], x <- [3 .. y], x^2 + y^2 == z^2]
-- take 5 pythaTriple
-- [(3,4,5),(6,8,10),(5,12,13),(9,12,15),(8,15,17)]
--

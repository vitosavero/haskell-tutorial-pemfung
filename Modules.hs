import Data.List

-- intersperse '.' "MONKEY"
-- "M.O.N.K.E.Y"
--

-- concat ["foo","bar","car"]
-- "foobarcar"
--

-- intersperse '.' "MONKEY" 
-- "M.O.N.K.E.Y"
--

-- intercalate " " ["hey","there","guys"]
-- "hey there guys"
--

-- transpose [[1,2,3],[4,5,6],[7,8,9]]
-- [[1,4,7],[2,5,8],[3,6,9]]
--

-- map sum $ transpose [[0,3,5,9],[10,0,0,9],[8,5,1,-1]]
-- [18,8,6,17]
--

-- concatMap (replicate 4) [1..3]
-- [1,1,1,1,2,2,2,2,3,3,3,3]
--

-- and $ map (>4) [5,6,7,8]
-- True
--

-- and $ map (==4) [4,4,4,3,4]
-- False
--

-- or $ map (==4) [2,3,4,5,6,1]
-- True
--

-- or $ map (>4) [1,2,3]
-- False
--

-- any (==4) [2,3,5,6,1,4]
-- True
--

-- all (>4) [6,9,10]
-- True
--
